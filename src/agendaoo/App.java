package agendaoo;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class App {

    public static void main(String[] args) {
        Agenda agenda = new Agenda();
       
        int op;
        String menu = "";
        try {
            Scanner scanner = new Scanner(new FileReader("contatos.txt"))
                    .useDelimiter("@#@");
            while (scanner.hasNext()) {
                String nome = scanner.next();
                String telefone = scanner.next();

                Pessoa p = new Pessoa(nome, telefone);

                agenda.adicionarContato(p);

            }

        } catch (IOException ex) {

            ex.printStackTrace();

        }

        do {
            menu = JOptionPane.showInputDialog("Agenda \n"
                    + "Contatos (" + agenda.getQuantidadeContatos() + ")\n"
                    + "1 - Novo\n"
                    + "2 - Deletar\n"
                    + "3 - Pesquisar\n"
                    + "4 - Listar Todos\n"
                    + "5 - Gravar agenda em disco\n"
                    + "6 - Sair\n"
            );
            op = Integer.parseInt(menu);
            try {
                switch (op) {
                    case 1:
                        menu = JOptionPane.showInputDialog("Nome");
                        String nome = menu;
                        if (agenda.ExisteNome(nome)) {
                            menu = JOptionPane.showInputDialog(agenda.VerificarNome(nome) + "\n"
                                    + "S/N?");
                            if (menu.equalsIgnoreCase("N")) {
                                break;
                            }
                        }
                        menu = JOptionPane.showInputDialog("Telefone");
                        String telefone = menu;
                        if (agenda.ExisteTelefone(telefone)) {
                            menu = JOptionPane.showInputDialog(agenda.VerificarTelefone(telefone) + "\n"
                                    + "S/N?");
                            if (menu.equalsIgnoreCase("N")) {
                                break;
                            }
                        }
                        Pessoa p = new Pessoa(nome, telefone);
                        agenda.VerificarContato(nome, telefone);
                        agenda.adicionarContato(p);
                        break;
                    case 2:
                        agenda.testListaVazia();
                        menu = JOptionPane.showInputDialog("Deletar Contato\n" + "Digite o Nome/Telefone");
                        agenda.removerContato(menu);
                        break;
                    case 3:
                        agenda.testListaVazia();
                        menu = JOptionPane.showInputDialog("Deseja buscar o contato por Nome ou Telefone?\n"
                                + "1 - Nome\n" + "2 - Telefone");
                        switch (menu) {
                            case "1":
                                menu = JOptionPane.showInputDialog("Digite o Nome a procurar");
                                JOptionPane.showMessageDialog(null, agenda.buscarContatoNome(menu));
                                break;
                            case "2":
                                menu = JOptionPane.showInputDialog("Digite o Telefone a procurar");
                                JOptionPane.showMessageDialog(null, agenda.buscarContatoTel(menu));
                                break;
                            default:
                                JOptionPane.showMessageDialog(null, "Comando inexistente!!!");
                                break;
                        }
                        break;
                    case 4:
                        JOptionPane.showMessageDialog(null, agenda.ListaFull());
                        break;
                    case 5:

                         agenda.gravar();
                      
                        break;
                    case 6:
                        System.exit(0);
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Comando inexistente!!!");
                        break;
                }
            } catch (RuntimeException ex) {
                JOptionPane.showMessageDialog(null, ex.getMessage());
            } catch (IOException EX) {

                EX.printStackTrace();
            }
        } while (true);
    }
}
